<?php

/**
 * Documentation for form chunk module hooks.
 */

/**
 * Return an array of form chunks.
 *
 * These chunks can be accessed via /admin/form-chunk/{chunk-name}/{entity_id}.
 *
 * The creator of the chunk is currently responsible for providing a link to
 * the chunk.
 *
 * @return array[]
 *   An array of form chunks, keyed by chunk name. Each chunk should have the
 *   following keys:
 *   - entity_type: The type of entity this chunk is for.
 *   - title: (optional) the title of the form.
 *   - fields: An array of field names, in the order they should appear on the form.
 */
function hook_form_chunks() {
  return array(
    'my-example-chunk' => array(
      'entity_type' => 'node',
      'title' => t('Edit example chunk for [node:title]'),
      'fields' => array(
        'field_example_1',
        'field_example_2',
        'field_example_3',
      )
    ),
  );
}
